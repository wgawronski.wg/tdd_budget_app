<?php

namespace Tests\Feature;

use \App\Models\Transaction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ViewTransactionTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_cant_display_all_transacion()
    {
        $transaction = Transaction::factory()->create();
        
        $this->get('/transactions')
            ->assertSee($transaction->description)
            ->assertSee($transaction->category->name)
            ;
        
    }
}
